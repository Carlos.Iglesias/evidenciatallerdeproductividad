# Evidencia de la clase de Taller de Productividad Basada en Herramientas Tecnológicas

Carlos Daniel Iglesias Rodríguez     2726784

El problema identificado en la empresa fue la baja integración tecnologica,
entre estos se encontró la falta de una página web con capacidad de realizar compras online
por lo que se propuso la realización de una primera entrega de esta.

# Requerimientos:
Base de datos en SQL.
Servidor dedicado para la página web y la base de datos (pueden ser compartidos o independientes).
Versión de Java 1.8 o superior.

# Instalación:
El ambiente de desarrollo se puede instalar descargando el repositorio, descargando las dependencias (Build with Dependencies) <br>
Y configurando los puertos en los que se desea que corra el programa. (Están configurados por default a 8080 FrontEnd y 8081 BackEnd)<br>
Las pruebas se pueden realizar utilizando algún software especializado siguiendo sus instrucciones.

# Configuración:
Corroborar que el archivo pom.xml estén todas las dependencias. Configurar en el archivo application.properties del BackEnd lo siguiente:

* El url de la base de datos este correctamente configurada con el nombre de tu base de datos.
* El username y password de la base de datos sean los correctos
* El mapping de hibernate este false
* Configurar el puerto donde se desea que corra el BackEnd (Ajustado por defecto al puerto 8081)
* Hay que especificar que el cache de thymeleaf sea false.


# Uso:
En un inicio el sistema puede correrse iniciando la sesión de debug en NetBeans/Eclipse/IDE seleccionado<br>
Hasta el 03/05/2020 no hay seguridad de usuarios, todos tienen el mismo role y permisos.

# Contribución:
Es un proyecto personal, pero se aceptan sugerencias o reportes de bugs/mejoras que pueden reportarse directamente en Git o a mi correo charly-iglesias@gmail.com<br>
El repositorio es público por lo que pueden clonar el repositorio desde la página principal con SSH o HTTP<br>
Creaciones de Branch están permitidas para sus proyectos pero pull requests serán ignorados.

# Roadmap:
Lo siguiente a realizar es agregar las restricciones de permisos por usuario e integrar una pasarela de pagos se está evaluando agregar la de OpenPay y PayPal.<br>
Así mismo el proceso de rastreo de pedidos y envió de notificaciones (correo).
